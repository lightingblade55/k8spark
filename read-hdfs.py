from pyspark.sql import SparkSession
from pyspark.sql.types import StructType

spark = SparkSession.builder.appName("PySpark Example - Read Parquet").getOrCreate()
spark.read.parquet("hdfs://namenode.hadoop:9000/user/hadoop/example1").show()
