from pytube import YouTube
from pyspark import SparkContext, SparkConf

conf = SparkConf().setAppName("Upload to Hadoop")
sc = SparkContext(conf=conf)

# initialize the YouTube object with the video URL
url = "https://www.youtube.com/watch?v=jNQXAC9IVRw"
yt = YouTube(url)

# get the first stream of the video with the highest resolution
stream = yt.streams.filter(progressive=True, file_extension='mp4').order_by('resolution').desc().first()

# get the download URL of the stream
url = stream.url
# print(url)
response1 = requests.get(url, stream=True)

base64_data = base64.b64encode(response1.content).decode('utf-8')

# Decode base64-encoded string to binary data
decoded_data = base64.b64decode(base64_data)

# Define the Hadoop file system URI and path

in_file='test.mp4'
fs_uri = "hdfs://namenode.hadoop:9000"
hadoop_path = "/user/hadoop/example1/%s"%(in_file)

# Define the local file path
local_path = "./%s"%(in_file)

# Use the Hadoop command to upload the file to HDFS
sc._jsc.hadoopConfiguration().set("fs.defaultFS", fs_uri)
sc._jsc.hadoopConfiguration().set("dfs.replication", "1")
hadoop_fs = sc._jvm.org.apache.hadoop.fs.FileSystem.get(sc._jsc.hadoopConfiguration())
hadoop_input_path = sc._jvm.org.apache.hadoop.fs.Path(hadoop_path)

if hadoop_fs.exists(hadoop_input_path):
    hadoop_fs.delete(hadoop_input_path, True)

hadoop_fs.copyFromLocalFile(sc._jvm.org.apache.hadoop.fs.Path(local_path), hadoop_input_path)
print(local_path)
print(hadoop_input_path)
